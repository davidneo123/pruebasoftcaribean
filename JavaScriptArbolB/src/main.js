//Arbol BST 
const { BinarySearchTree } = require('./common/binary-search-tree');
const BST = new BinarySearchTree();

BST.insert(15);
BST.insert(25);
BST.insert(10);
BST.insert(7);
BST.insert(22);
BST.insert(17);
BST.insert(13);
BST.insert(5);
BST.insert(9);
BST.insert(27);

const root = BST.getRootNode();
let arrayResults = [];
BST.inOrder(root, (node) => arrayResults.push(node.data));
console.log(arrayResults);

arrayResults = [];
BST.preOrder(root, (node) => arrayResults.push(node.data));
console.log(arrayResults);

arrayResults = [];
BST.postOrder(root, (node) => arrayResults.push(node.data));
console.log(arrayResults);

// console.log(BST.searchNode(root, 17));
// console.log(BST.getRootNode())

console.log('En total hay: ' + BST.getCant() + ' nodos')