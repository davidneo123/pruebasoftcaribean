class DoublyLinkedListNode {
    constructor(data) {
        this.data = data;
        this.next = null;
        this.previous = null;
    }
}
// create the first node
const nuevo = new DoublyLinkedListNode('primero');

// add a second node
const secondNode = new DoublyLinkedListNode('segundo');
nuevo.next = secondNode;
secondNode.previous = nuevo;




let current = nuevo;

while (current !== null) {
    console.log('Fila inicial: ' + current.data);
    current = current.next;
}



const thirdNode = new DoublyLinkedListNode('colado');

thirdNode.previous = secondNode.previous;
secondNode.previus = nuevo.previous;
nuevo.next = thirdNode;


let ultimo = thirdNode;

while (ultimo !== null) {
    console.log('Despues del colado: ' + ultimo.data);
    ultimo = ultimo.previous;
}

const head = Symbol("head");
const tail = Symbol("tail");

class DoublyLinkedList {

    constructor() {
        this[head] = null;
        this[tail] = null;
    }

    add(data) {

        // create the new node and place the data in it
        const newNode = new DoublyLinkedListNode(data);

        // special case: no nodes in the list yet
        if (this[head] === null) {
            this[head] = newNode;
        } else {

            // link the current tail and new tail
            this[tail].next = newNode;
            newNode.previous = this[tail];
        }

        // reassign the tail to be the new node
        this[tail] = newNode;
    }
    get(index) {

        // ensure `index` is a positive value
        if (index > -1) {

            // the pointer to use for traversal
            let current = this[head];

            // used to keep track of where in the list you are
            let i = 0;

            // traverse the list until you reach either the end or the index
            while ((current !== null) && (i < index)) {
                current = current.next;
                i++;
            }

            // return the data if `current` isn't null
            return current !== null ? current.data : undefined;
        } else {
            return undefined;
        }
    }
}

const list = new DoublyLinkedList();
//list.add("otro");