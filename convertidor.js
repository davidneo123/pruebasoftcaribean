const inquirer = require('inquirer')

var number = [{
    type: 'input',
    name: 'metros',
    message: "Por favor introduzca el número de metros",
}]
var pies = 3.28084
var pul = 39.37
inquirer.prompt(number).then(res => {
    let num = Number(res['metros'])
    console.log(`${num} Metros es igual a ${pul * num} pulgadas y a ${pies * num} pies`)

})