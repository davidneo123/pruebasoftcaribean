// getNum(1) = 1
// getNum(2) = 2

const inquirer = require('inquirer')

var number = [{
    type: 'input',
    name: 'number',
    message: "Por favor introduzca un número mayor o igual a 3",
}]

function getNumber(num) {
    var numero = 0
    for (i = 1; i <= num; i++) {
        i < 3 ? numero = 1 : numero = (i - 1) + (i - 2)
        console.log(numero)
    }
    return numero
}

function getFibo(num) {
    var numero = []
    console.log(numero[1])
    for (i = 0; i <= num; i++) {
        pos = i - 1
        i < 2 ? numero[i] = 1 : numero[i] = Number(numero[pos]) + Number(numero[pos - 1])
        console.log(numero)
    }
    console.log(numero[num])
    return numero[num]
}
inquirer.prompt(number).then(res => {
    let num = Number(res['number'])
    if (num > 2) {
        var number = getFibo(num)
        console.log('resultado: ' + number)
    } else { console.log('Menor a 3') }
})