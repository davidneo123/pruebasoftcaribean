var fs = require('fs')

function isPrimo(numero) {

    for (var i = 2; i < numero; i++) {

        if (numero % i === 0) {
            return false;
        }

    }

    return numero !== 1;
}
var sumaPrimos = 0
var sumaPares = 0
fs.readFile('numbers.txt', 'utf8', function(err, data) {
    console.log(data)
    let linea = data.replace('\n', '').split(',')
    linea.forEach(num => {
            console.log(Number(num))
            let number = Number(num)
            if (isPrimo(number)) {
                sumaPrimos = sumaPrimos + number
            }
            if (Number.isInteger(number / 2)) {
                sumaPares = sumaPares + number
            }
        })
        //    linea > 0 ? this.size = this.size + 1 : null
    console.log(sumaPares, ' primos:', sumaPrimos)
    if (err != null) { console.log(`No se encontro el archivo. ${err}`) }
})

function primo(numero) {
    for (var i = 2; i < numero; i++) {
        if (numero % i === 0) {
            return false;
        }
    }
    return numero !== 1;
}