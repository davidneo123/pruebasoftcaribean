const inquirer = require('inquirer')

var number = [{
    type: 'input',
    name: 'number',
    message: "Por favor introduzca un número",
}]

function cleanInt(x) {
    x = Number.parseInt(x);
    var a = x >= 0 ? Math.floor(x) : Math.ceil(x);
    return a
}

function isMulti(x, y) {
    if (Number.isInteger(Number(x) / Number(y))) {
        return true;
    }
    return false;
}

inquirer.prompt(number).then(res => {
    let num = cleanInt(res['number'])
    if (isMulti(num, 4)) {
        console.log(`${res['number']} Es múltiplo de 4! Los múltiplos son:`)
        console.log(num)
        for (i = 1; i <= (num / 4); i++) {
            num = num - 4
            console.log(num)
        }
    } else {
        console.log(res['number'], ' No es multiplo de 4')
    }
})