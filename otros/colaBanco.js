class DoublyLinkedListNode {
    constructor(data) {
        this.data = data;
        this.next = null;
        this.previous = null;
    }
}
// create the first node
const nuevo = new DoublyLinkedListNode('primero');

// add a second node
const secondNode = new DoublyLinkedListNode('segundo');
nuevo.next = secondNode;
secondNode.previous = nuevo;

// add a third node
const thirdNode = new DoublyLinkedListNode('tercero');
secondNode.next = thirdNode;
thirdNode.previous = secondNode;

const cola = thirdNode;

let current = nuevo;

while (current !== null) {
    console.log(current.data);
    current = current.next;
}

let ultimo = cola;

while (ultimo !== null) {
    console.log(ultimo.data);
    ultimo = ultimo.previous;
}
const head = Symbol("head");
const tail = Symbol("tail");

class DoublyLinkedList {

    constructor() {
        this[head] = null;
        this[tail] = null;
    }

    add(data) {

        // create the new node and place the data in it
        const newNode = new DoublyLinkedListNode(data);

        // special case: no nodes in the list yet
        if (this[head] === null) {
            this[head] = newNode;
        } else {

            // link the current tail and new tail
            this[tail].next = newNode;
            newNode.previous = this[tail];
        }

        // reassign the tail to be the new node
        this[tail] = newNode;
    }
    get(index) {

        // ensure `index` is a positive value
        if (index > -1) {

            // the pointer to use for traversal
            let current = this[head];

            // used to keep track of where in the list you are
            let i = 0;

            // traverse the list until you reach either the end or the index
            while ((current !== null) && (i < index)) {
                current = current.next;
                i++;
            }

            // return the data if `current` isn't null
            return current !== null ? current.data : undefined;
        } else {
            return undefined;
        }
    }
}

const list = new DoublyLinkedList();
list.add("red");
list.add("orange");
list.add("yellow");
//console.log(list)
// get the second item in the list
for (i = 0; i < list.size; i++) { console.log('Elemento: ' + list.get(i)) }; // "orange"



/* function Fila(nuevo) {
    this.nuevo = nuevo
    this.proximo = null
}

function Cola() {
    this.punteador = null
}

Cola.prototype.hacerCola = function(nuevo, actual = this.punteador) {
    if (this.punteador === null) {
        return this.punteador = new Fila(nuevo)
    }
    if (actual.proximo === null) {
        return actual.proximo = new Fila(nuevo)
    }
    this.hacerCola(nuevo, actual.proximo)
}

Cola.prototype.personas = function(actual = this.punteador, acum = 1) {
    if (this.proximo === null) {
        return 0
    }
    if (actual.proximo !== null) {
        return this.personas(actual.proximo, acum = acum + 1)
    }
    return acum
}
Cola.prototype.colarse = function(nuevo) {
    if (this.punteador === null) {
        return this.punteador = new Fila(nuevo)
    }
    let nuevoColado = new Fila(nuevo)
    nuevoColado.proximo = this.punteador
    this.punteador = nuevoColado
}


cola = new Cola()
console.log(cola.hacerCola('dav'))
console.log(cola.hacerCola('dav2'))
console.log(cola.hacerCola('dav3'))
console.log(cola.colarse('Colado'))
console.log(cola.personas()) */