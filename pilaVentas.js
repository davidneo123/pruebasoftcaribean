class Ventas {
    constructor() {
        this.cant = 0
        this.venta = {}
    }

    add(item) {
        this.venta[this.cant] = item
        this.cant++
            return this.venta
    }

    remove() {
        this.cant--
            const item = this.venta[this.cant]
        delete this.venta[this.cant]
        return item
    }

    last() {
        return this.venta[this.cant - 1]
    }

    info() {
        for (var i = 0; i < this.cant; i++) {

        }
        console.log('Cantidad de ventas: ' + this.cant + '\n Detalle última venta: \n ' + this.venta[this.cant - 1])
    }

}
const ventas = new Ventas()
console.log(ventas.add('venta 1'))
console.log(ventas.add('venta 2'))
console.log(ventas.add('venta 3'))
ventas.info()